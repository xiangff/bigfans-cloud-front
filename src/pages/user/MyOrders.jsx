/**
 * Created by lichong on 2/27/2018.
 */
import React from 'react';

import {Row, Col, Card, Divider, Icon, Popover} from 'antd';
import TopBar from '../../components/TopBar';
import FootBar from '../../components/FootBar';
import CenterMenu from '../../components/UserCenterMenus'

import HttpUtils from 'utils/HttpUtils'

import {Link} from 'react-router-dom'

class MyOrders extends React.Component {

    state = {
        orders: []
    }

    componentDidMount(){
        let self = this;
        HttpUtils.listMyOrders({
            success(resp){
                self.setState({orders : resp.data})
            },
            error(){

            }
        })
    }

    render() {
        return (
            <div>
                <Row>
                    <Col span={3}/>
                    <Col span={18}>
                        <TopBar/>
                    </Col>
                    <Col span={3}/>
                </Row>
                <Divider style={{margin: '0'}}/>
                <Row>
                    <Col span={3}/>
                    <Col span={18}>
                        <Row gutter={15}>
                            <Col span={5}>
                                <CenterMenu currentMenu="myorders"/>
                            </Col>
                            <Col span={19}>
                                {
                                    this.state.orders.map((order , index) => {
                                        return (
                                            <div style={{marginTop:'30px'}}>
                                                <Card
                                                    type="inner"
                                                    title={this.renderTitle(order)}
                                                >
                                                    <Col span={10}>
                                                        {
                                                            order.items.map((item , index) => {
                                                                return (
                                                                    <Row style={{marginTop:'10px'}} key={index}>
                                                                        <img style={{width:'60px' , height:'60px'}} className="pull-left" src={item.prodImg}/>
                                                                        <span>
                                                                            <Link style={{marginTop:'0px'}} to={"/product/" + item.prodId} target="_blank">{item.prodName}</Link>
                                                                        </span>
                                                                        <span className="pull-right"> x {item.quantity}</span>
                                                                    </Row>
                                                                )
                                                            })
                                                        }
                                                    </Col>
                                                    <Col span={3} className='text-center'>
                                                        <Row>
                                                            <Popover
                                                                content={order.addressDetail}
                                                                trigger="hover"
                                                                placement="bottomLeft"
                                                            >
                                                                <Icon type="contacts"/>
                                                            </Popover>
                                                            <p>{order.addressConsignee}</p>
                                                        </Row>
                                                    </Col>
                                                    <Col span={4} className='text-center'>
                                                        <Row>总金额: {order.totalPrice}</Row>
                                                        <Divider/>
                                                        <Row>付款方式: 支付宝</Row>
                                                    </Col>
                                                    <Col span={4} className='text-center'>
                                                        <Row>订单状态: {order.status}</Row>
                                                        <Row><Link to={"/orders/" + order.id} target="_blank">订单详情</Link></Row>
                                                    </Col>
                                                    <Col span={2} className='text-center'>
                                                        <Row>aaaa</Row>
                                                        <Row><Link to={'/comment?orderId=' + order.id} target='_blank'>去评价</Link></Row>
                                                    </Col>
                                                </Card>
                                            </div>
                                        )
                                    })
                                }
                            </Col>
                        </Row>
                    </Col>
                    <Col span={3}/>
                </Row>
                <Row>
                    <Col span={3}/>
                    <Col span={18}>
                        <FootBar/>
                    </Col>
                    <Col span={3}/>
                </Row>
            </div>
        )
    }

    renderTitle(order) {
        return (
            <div>
                <span>{order.createDateStr}</span>
                <span style={{marginLeft : '30px'}}>
                    订单号: <Link to={"/orders/" + order.id} target='_black'>{order.id}</Link>
                </span>
            </div>
        )
    }
}

export default MyOrders;