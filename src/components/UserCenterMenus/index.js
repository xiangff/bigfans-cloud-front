/**
 * Created by lichong on 2/27/2018.
 */
import React from 'react';

import {Row, Col, Menu, Icon, Divider} from 'antd';

import {Link} from 'react-router-dom'

const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

class CenterMenu extends React.Component {

    handleClick = (e) => {
        console.info("click: " + e);
    }

    render() {
        return (
            <div>
                <Menu
                    onClick={(e) => this.handleClick(e)}
                    defaultSelectedKeys={[this.props.currentMenu]}
                    defaultOpenKeys={['sub1']}
                    mode="inline"
                >
                    <SubMenu key="sub1">
                        <MenuItemGroup key="g1" title="订单中心">
                            <Menu.Item key="myorders"><Link to="/myorders">我的订单</Link></Menu.Item>
                            <Menu.Item key="comment">晒单评价</Menu.Item>
                        </MenuItemGroup>
                        <MenuItemGroup key="g2" title="个人中心">
                            <Menu.Item key="me">个人信息</Menu.Item>
                            <Menu.Item key="myaddress"><Link to="/myaddress">收货地址</Link></Menu.Item>
                            <Menu.Item key="mycoupons"><Link to="/mycoupons">优惠券</Link></Menu.Item>
                            <Menu.Item key="account">账户安全</Menu.Item>
                            <Menu.Item key="sub">邮件/短信订阅</Menu.Item>
                        </MenuItemGroup>
                        <MenuItemGroup key="g3" title="关注中心">
                            <Menu.Item key="favor">我的收藏</Menu.Item>
                            <Menu.Item key="history">浏览历史</Menu.Item>
                        </MenuItemGroup>
                    </SubMenu>
                </Menu>
            </div>
        )
    }
}

export default CenterMenu;